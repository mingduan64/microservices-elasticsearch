package com.example.productorderingservice.services;

import com.example.productorderingservice.models.CancelProductOrder;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;

import java.util.List;
@Service
public interface CancelProductOrderService {
   CancelProductOrder save(CancelProductOrder cancelProductOrder);

    void delete(CancelProductOrder cancelProductOrder);

//    CancelProductOrder findOne(String id);

    Iterable<CancelProductOrder> findAll();

    Page<CancelProductOrder> findByProductOrderNotNull(String productOrder, PageRequest pageRequest);

    List<CancelProductOrder> findByAtTypeNotNull(String atType);
}
