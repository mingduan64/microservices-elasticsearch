package com.example.productorderingservice.services.impl;

import com.example.productorderingservice.models.CancelProductOrder;
import com.example.productorderingservice.repositories.CancelProductOrderRepository;
import com.example.productorderingservice.services.CancelProductOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;

import java.util.List;
@Service
public class CancelProductOrderServiceImpl implements CancelProductOrderService {
    @Autowired
    private CancelProductOrderRepository repository;
    @Override
    public CancelProductOrder save(CancelProductOrder CancelProductOrder) {
        return repository.save(CancelProductOrder);
    }
    @Override
    public void delete(CancelProductOrder CancelProductOrder) {
        repository.delete(CancelProductOrder);
    }

//    public CancelProductOrder findOne(String id) {
//        return repository.findOne(id);
//    }
    @Override
    public Iterable<CancelProductOrder> findAll() {
        return repository.findAll();
    }
    @Override
    public Page<CancelProductOrder> findByProductOrderNotNull(String productOrder, PageRequest pageRequest) {
        return repository.findByProductOrderNotNull(productOrder, pageRequest);
    }
    @Override
    public List<CancelProductOrder> findByAtTypeNotNull(String atType) {
        return repository.findByAtTypeNotNull(atType);
    }

}
