package com.example.productorderingservice.repositories;

import com.example.productorderingservice.models.CancelProductOrder;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CancelProductOrderRepository extends ElasticsearchRepository<CancelProductOrder,String> {
    Page<CancelProductOrder> findByProductOrderNotNull(String productOrder, Pageable pageable);

    List<CancelProductOrder> findByAtTypeNotNull(String atType);

//    CancelProductOrder findOne(String id);
}
