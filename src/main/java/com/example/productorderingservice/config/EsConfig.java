package com.example.productorderingservice.config;

import org.elasticsearch.action.index.IndexRequest;
import org.elasticsearch.action.index.IndexResponse;
import org.elasticsearch.client.RequestOptions;
import org.elasticsearch.client.RestClient;
import org.elasticsearch.client.RestHighLevelClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.elasticsearch.client.ClientConfiguration;
import org.springframework.data.elasticsearch.client.RestClients;
import org.springframework.data.elasticsearch.config.AbstractElasticsearchConfiguration;
import org.springframework.data.elasticsearch.core.ElasticsearchOperations;
import org.springframework.data.elasticsearch.core.ElasticsearchRestTemplate;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;

import java.io.IOException;
import java.util.UUID;

import static java.util.Collections.singletonMap;
import static org.elasticsearch.action.support.WriteRequest.RefreshPolicy.IMMEDIATE;

@Configuration

@EnableAutoConfiguration
@EnableElasticsearchRepositories(basePackages = "com.example.productorderingservice.repositories")
@ComponentScan(basePackages = { "com.example.productorderingservice" })

public class EsConfig {

//    public EsConfig() throws IOException {
//    }
//
//    @Override
//    @Bean
//    public RestHighLevelClient elasticsearchClient() {
//
//        final ClientConfiguration clientConfiguration = ClientConfiguration.builder()
//                .connectedTo("localhost:9200")
//                .build();
//
//        return RestClients.create(clientConfiguration).rest();
//    }
//
//
//// ...
//
//    @Autowired
//    RestHighLevelClient highLevelClient;
//
//    RestClient lowLevelClient = highLevelClient.getLowLevelClient();
//
//// ...
//
//    IndexRequest request = new IndexRequest("spring-data")
//            .id(randomID())
//            .source(singletonMap("feature", "high-level-rest-client"))
//            .setRefreshPolicy(IMMEDIATE);
//
//    IndexResponse response = highLevelClient.index(request, RequestOptions.DEFAULT);
//    public static String randomID() {
//        return UUID.randomUUID().toString();
//    }
@Bean
public RestHighLevelClient client() {
    ClientConfiguration clientConfiguration
            = ClientConfiguration.builder()
            .connectedTo("localhost:9200")
            .build();

    return RestClients.create(clientConfiguration).rest();
}

    @Bean
    public ElasticsearchOperations elasticsearchTemplate() {
        return new ElasticsearchRestTemplate(client());
    }
}

