package com.example.productorderingservice.controllers;

import com.example.productorderingservice.models.CancelProductOrder;
import com.example.productorderingservice.repositories.CancelProductOrderRepository;
import com.example.productorderingservice.services.CancelProductOrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/tmf-api/productOrderingManagement/v4/cancelProductOrder")
public class CancelProductOrderController {
    @Autowired
    private CancelProductOrderService cancelProductOrderService;
    @Autowired
    private CancelProductOrderRepository cancelProductOrderRepository;

//    @PostMapping("/addBook")
//    public String saveBook(@RequestBody CancelProductOrder CancelProductOrder){
//        repo.save(book);
//
//        return "Added Successfully";
//    }

    @GetMapping
    public Iterable<CancelProductOrder> getAll() {
        return cancelProductOrderService.findAll();
    }


    @PostMapping
    public ResponseEntity<CancelProductOrder> save(@RequestBody CancelProductOrder cancelProductOrder) {
        CancelProductOrder savedCancelProductOrder = cancelProductOrderService.save(cancelProductOrder);
        return ResponseEntity.ok().body(savedCancelProductOrder);
    }


    @DeleteMapping("/{id}")
    public String deleteCancelProductOrder(@PathVariable String id){
        cancelProductOrderRepository.deleteById(id);

        return "Deleted Successfully";
    }

}
