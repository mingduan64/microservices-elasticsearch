package com.example.productorderingservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.elasticsearch.repository.config.EnableElasticsearchRepositories;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;


@SpringBootApplication
@EnableWebMvc
//@EnableMongoRepositories
//@EnableElasticsearchRepositories
public class ProductOrderingServiceApplication {

    public static void main(String[] args) {
        SpringApplication.run(ProductOrderingServiceApplication.class, args);
    }

}
